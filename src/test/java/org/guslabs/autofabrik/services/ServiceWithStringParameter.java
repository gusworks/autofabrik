package org.guslabs.autofabrik.services;

import org.guslabs.autofabrik.Callable;
import org.guslabs.autofabrik.helpers.StringParameter;

public class ServiceWithStringParameter implements Callable<StringParameter, String> {

	public String call(StringParameter parameter) {
		return parameter.toString();
	}

}
