package org.guslabs.autofabrik;

import static org.junit.Assert.assertEquals;

import java.io.File;
import org.guslabs.autofabrik.exception.ServiceNotFoundException;
import org.guslabs.autofabrik.helpers.StringParameter;
import org.junit.Before;
import org.junit.Test;

public class AutoFabrikTest {

	private static final String DESCRIPTOR_FILE = "src/test/resources/service-descriptor.xml";
	private AutoFabrik<StringParameter, String> dispatcher;

	@Before
	public void setUp() throws Exception {
		dispatcher = new AutoFabrik<StringParameter, String>(DESCRIPTOR_FILE);
	}

    @Test
    public void createDispatcherWithoutArguments() throws Exception {
        dispatcher = new AutoFabrik<StringParameter, String>();
    }

	@Test
	public void executeTestServiceWithNullParametersAndReturnOK() throws Exception {
		assertEquals("OK", dispatcher.execute("TEST", new StringParameter(null)));
	}

	@Test
	public void executeTestServiceWithParametersAndReturnOK() throws Exception {
		assertEquals("yeah!", dispatcher.execute("TEST", new StringParameter("yeah!")));
	}

	@Test(expected = ServiceNotFoundException.class)
	public void executeUnknownServiceThrowsException() throws Exception {
		assertEquals("yeah!", dispatcher.execute("UNKNOWN", new StringParameter("yeah!")));
	}

	@Test
	public void haveSomeFunWithURI(){
		System.out.println(new File(DESCRIPTOR_FILE).toURI());
	}
}