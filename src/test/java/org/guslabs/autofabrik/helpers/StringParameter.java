package org.guslabs.autofabrik.helpers;

public class StringParameter {
	private final String value;

	public StringParameter(final String value) {
		this.value = value;
	}

	public String toString() {
		return value == null ? "OK" : value;
	}
}
