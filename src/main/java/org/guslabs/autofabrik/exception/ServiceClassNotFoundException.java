package org.guslabs.autofabrik.exception;

public class ServiceClassNotFoundException extends AutoFabrikException {

	public ServiceClassNotFoundException(final Exception e) {
		super(e);
	}

}
