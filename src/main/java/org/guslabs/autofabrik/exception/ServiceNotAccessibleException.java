package org.guslabs.autofabrik.exception;

public class ServiceNotAccessibleException extends AutoFabrikException {

	public ServiceNotAccessibleException(final IllegalAccessException e) {
		super(e);
	}

}
