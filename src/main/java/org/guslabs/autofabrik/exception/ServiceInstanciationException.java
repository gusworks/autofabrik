package org.guslabs.autofabrik.exception;

public class ServiceInstanciationException extends AutoFabrikException {

	public ServiceInstanciationException(final InstantiationException e) {
		super(e);
	}

}
