package org.guslabs.autofabrik.exception;

/**
 * @author PBalduino
 *
 */
public class AutoFabrikException extends RuntimeException {

	public AutoFabrikException() {
		super();
	}

	public AutoFabrikException(final Exception e) {
		super(e);
	}

	public AutoFabrikException(final String message) {
		super(message);
	}

}
