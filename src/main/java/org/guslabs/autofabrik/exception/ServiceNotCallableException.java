package org.guslabs.autofabrik.exception;

public class ServiceNotCallableException extends AutoFabrikException {

	public ServiceNotCallableException(final Exception e) {
		super(e);
	}
}