package org.guslabs.autofabrik.exception;

public class ServiceNotFoundException extends AutoFabrikException {

	public ServiceNotFoundException(final String message) {
		super(message);
	}

}