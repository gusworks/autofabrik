package org.guslabs.autofabrik;

import org.guslabs.autofabrik.exception.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;

public class AutoFabrik<PARAMETER, RETURN> {

	private final HashMap<String, Class<Callable<PARAMETER, RETURN>>> services;

	public AutoFabrik() throws Exception {
        InputStream stream = getClass().getResourceAsStream("/service-descriptor.xml");
		services = new ServiceLoader<PARAMETER, RETURN>(stream).load();
	}

	public AutoFabrik(final String serviceDescriptor) throws Exception {
		services = new ServiceLoader<PARAMETER, RETURN>(new FileInputStream(serviceDescriptor)).load();
	}

	public final RETURN execute(final String serviceName, final PARAMETER parameters) {
		final Class<Callable<PARAMETER, RETURN>> service = services.get(serviceName);

		if (service != null) {
			try {
				return (service.newInstance()).call(parameters);
			} catch (final InstantiationException e) {
				throw new ServiceInstanciationException(e);
			} catch (final IllegalAccessException e) {
				throw new ServiceNotAccessibleException(e);
			} catch (final ClassCastException e) {
				throw new ServiceNotCallableException(e);
			} catch (final Exception e){
				throw new AutoFabrikException(e);
			}
		}

		throw new ServiceNotFoundException("Service " + serviceName + " not found");
	}
}