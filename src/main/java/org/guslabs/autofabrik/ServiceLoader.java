package org.guslabs.autofabrik;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.util.HashMap;

final class ServiceLoader<PARAMETER, RETURN> {
	private final InputStream descriptorInputStream;

	ServiceLoader(final InputStream inputStream){
		this.descriptorInputStream = inputStream;
	}

	public HashMap<String, Class<Callable<PARAMETER, RETURN>>> load() throws Exception {
		final HashMap<String, Class<Callable<PARAMETER, RETURN>>> hash = new HashMap<String, Class<Callable<PARAMETER, RETURN>>>();

		final SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();

		final ServiceDefaultHandler<PARAMETER, RETURN> defaultHandler = new ServiceDefaultHandler<PARAMETER, RETURN>(hash);

        saxParser.parse(descriptorInputStream, defaultHandler);

		return defaultHandler.getHash();
	}

}
