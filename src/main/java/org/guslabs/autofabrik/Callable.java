package org.guslabs.autofabrik;

public interface Callable<PARAMETER, RETURN> {

	RETURN call(PARAMETER parameter) throws Exception;

}