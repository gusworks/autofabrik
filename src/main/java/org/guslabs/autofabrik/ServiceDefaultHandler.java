package org.guslabs.autofabrik;

import org.guslabs.autofabrik.exception.ServiceClassNotFoundException;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;

final class ServiceDefaultHandler<PARAMETER, RETURN> extends DefaultHandler {
	private boolean readingName;
	private boolean readingService;
	private final HashMap<String, Class<Callable<PARAMETER, RETURN>>> hash;
	private String service;
	private String name;

	public ServiceDefaultHandler(final HashMap<String, Class<Callable<PARAMETER, RETURN>>> hash) {
		this.hash = hash;
	}

	@Override
	public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("name")) {
			readingName = true;
		} else if (qName.equalsIgnoreCase("class")) {
			readingService = true;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void endElement(final String uri, final String localName, final String qName) {
		if (qName.equalsIgnoreCase("service")) {
			try {
				hash.put(name, (Class<Callable<PARAMETER, RETURN>>) Class.forName(service));
			} catch (final ClassNotFoundException e) {
				throw new ServiceClassNotFoundException(e);
			} finally {
				name = null;
				service = null;
			}
		}
	}

	@Override
	public void characters(final char ch[], final int start, final int length) throws SAXException {
		if(readingName){
			name = new String(ch, start, length);
			readingName = false;
		} else if(readingService){
			service = new String(ch, start, length);
			readingService = false;
		}
	}

	public HashMap<String, Class<Callable<PARAMETER, RETURN>>> getHash(){
		return hash;
	}
}